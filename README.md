**DISCLAIMER:** This project is not activley maintained anymore. Don't expect bugs to be fixed, or features to be implemented. 

<p align='center'>
<img src='app/src/main/ic_launcher-web.png' height='150'></img>
</p>

<h1 align='center'>SplitBills</h1>
<h4 align='center'>A Android app, that helps you and your friends to split your collective expenses.</h4>

<p align='center'>
<a href='https://f-droid.org/packages/org.weilbach.splitbills/'>
<img alt='Get it on F-Droid' src='https://fdroid.gitlab.io/artwork/badge/get-it-on.png' height='75'>
</a>

<a href='https://play.google.com/store/apps/details?id=org.weilbach.splitbills'>
<img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png' height='75'/>
</a>
</p>

<p align='center'>
<a href="https://www.gnu.org/licenses/gpl-3.0" alt="License: GPLv3"><img src="https://img.shields.io/badge/License-GPL%20v3-blue.svg"></a>
</p>
<hr>

## About

Use SplitBills to split bills with your friends or other people. You can create groups with people, add bills to the group and view your balances. SplitBills works without any server or registration and is free software. Your expenses belong to you!

If you have any problems with the app please contact me or open a issue here, rather than leave a one-star review: https://gitlab.com/flexw/splitbills/issues


[Privacy policy](PRIVACY_POLICY.md)


[License](LICENSE)

## Features

* Split bills with other people
* Sync your expenses with other people
* View balances
* No registration needed
* Dezentral
* Custom split

## Screenshots

### Phone

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1_en-US.png" alt="drawing" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2_en-US.png" alt="drawing" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/3_en-US.png" alt="drawing" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4_en-US.png" alt="drawing" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/5_en-US.png" alt="drawing" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/6_en-US.png" alt="drawing" width="200"/>
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/7_en-US.png" alt="drawing" width="200"/>
